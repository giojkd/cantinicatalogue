<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cantiniproduct extends Model
{
  protected $guarded = [];

  public function attributeImport()
  {
    return $this->hasMany('App\Cantiniattributeimport');
  }

  public function texts()
  {
    return $this->morphMany('App\Cantinitext', 'textable');
  }


  public function attributevalues()
  {
    return $this->belongsToMany('App\Cantiniattributevalue');
  }

  public function photos(){
    return $this->hasMany('App\Cantinifile')->where('type','image');
  }

  public function tech_sheets(){
    return $this->hasMany('App\Cantinifile')->where('type','tech_sheet');
  }

}
