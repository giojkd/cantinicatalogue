<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Products extends Controller
{
  public function getProducts(Request $request){
    $photosPrefix = 'http://cantinicatalogue.brandboosterdemo.com/uploads/';

    $data = $request->all();
    $lang = ($data['lang'] != '') ? $data['lang'] : 'it';


    $products = \App\Cantiniurl::with([
      'products',
      'products.attributevalues',
      'products.attributevalues.texts',
      'products.attributevalues.attribute',
      'products.attributevalues.attribute.texts',
      'products.photos',
      'products.tech_sheets'
      ])->take(10)->get();

      #print_r($products);

      foreach($products as $product){
        $aux = [];
        $aux = [
          'id' => $product->id,
          'name' => $product->name,
          'hide' => false
        ];
        $versionIndex = 0;
        $attribute_values_ids = [];
        foreach($product->products as $product_){

          $version = [];
          $photos = [];
          $tech_sheets = [];
          $attributes = [];
          foreach($product_->photos as $photo){
            $photos[] = $photosPrefix.$photo->path;
          }
          foreach($product_->tech_sheets as $file){
            $tech_sheets[] = $photosPrefix.$file->path;
          }


          foreach($product_->attributevalues as $attribute_value){
            $attribute_values_ids[] = $attribute_value->id;
            $attribute_ = [];
            $attribute = $attribute_value->attribute;
            $attribute_ = [
              'id' => $attribute->id
            ];
            $currentAttribute = '';

            if(count($attribute->texts)>0){
              foreach($attribute->texts as $text){
                if($text->lang == $lang){
                  $attribute_['texts'][$text->text_type] = $text->text;
                  $currentAttribute = $text->text;
                }

              }
            }

            $attribute_['values'][$attribute_value->id]['id'] = $attribute_value->id;
            if(count($attribute_value->texts)>0){
              foreach($attribute_value->texts as $text){
                if($text->lang == $lang){
                  $attribute_['values'][$attribute_value->id]['texts'][$text->text_type] = $text->text;
                  #hack to get product category
                  if($currentAttribute == 'Settore'){
                    $aux['category'] = $text->text;
                  }
                  #############################
                }

              }
            }
            $attributes[$attribute->id] = $attribute_;
          }
          $version = [
            'photos' => $photos,
            'tech_sheets' => $tech_sheets,
            'name' => $product_->version,
            'attributes' => $attributes,
            'index' => $versionIndex,

          ];
          $aux['versions'][] = $version;
          $versionIndex++;
        }
        $aux['attribute_values_ids'] = \array_values(\array_unique($attribute_values_ids));
        $return[] = $aux;
      }

      #dd($return);


      return array_chunk($return, 3, false);
    }

    public function getProductAttributes(Request $request){


      $data = $request->all();



      $lang = ($data['lang'] != '') ? $data['lang'] : 'it';
      $attributes = \App\Cantiniattribute::with('texts','values','values.texts')->get();

      foreach($attributes as $attribute){
        $aux = [];
        $aux['id'] = $attribute->id;
        foreach($attribute->texts as $text){
          if($text->lang == $lang)
          $aux['texts'][$text->text_type] = $text->text;
        }
        foreach($attribute->values as $value){
          $aux['values'][$value->id]['id'] = $value->id;
          foreach($value->texts as $text){
            if($text->lang == $lang)
            $aux['values'][$value->id]['texts'][$text->text_type] = $text->text;
          }
          $keys = array_map(function($val) { return $val['texts']['name']; }, $aux['values']);
          array_multisort($keys, $aux['values']);
        }
        $return[] = $aux;
      }

      /* echo '<pre>';
      print_r($return);
      exit; */
      return $return;

    }
  }
