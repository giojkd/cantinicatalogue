<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cantinitext extends Model
{
  //
    protected $guarded = [];
  public function textable()
  {
    return $this->morphTo();
  }
}
