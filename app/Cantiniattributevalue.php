<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cantiniattributevalue extends Model
{
  //
  protected $guarded = [];


  public function attribute()
  {
    return $this->belongsTo('App\Cantiniattribute','cantiniattribute_id');
  }

  public function texts()
  {
    return $this->morphMany('App\Cantinitext', 'textable')->orderBy('text','ASC');
  }


  public function products()
  {
    return $this->belongsToMany('App\Cantiniproduct');
  }
}
