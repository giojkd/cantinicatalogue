<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cantiniattribute extends Model
{
    //
    
    protected $guarded = [];
    public function texts()
     {
         return $this->morphMany('App\Cantinitext', 'textable')->orderBy('text');
     }
     public function values()
      {
          return $this->hasMany('App\Cantiniattributevalue');
      }

}
